import sys
from math import ceil

d = []
total = 0
for line in sys.stdin:
	words = line.split()
	d.append((words[0],int(words[1])))
	total+=int(words[1])
	
for k,v in d:
	per = v*100/total
	per = ceil(per)
	print(k,'\t','[','*'*((per+4)//5),'] ',per,'%',sep='')
